package com.yoesuv.researchviewpager.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.yoesuv.researchviewpager.views.FragmentSlider

class ViewPagerAdapter(fragmentActivity: FragmentActivity, private val data: List<String>) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int {
        return data.size
    }

    override fun createFragment(position: Int): Fragment {
        return FragmentSlider.getInstance(data[position])
    }
}