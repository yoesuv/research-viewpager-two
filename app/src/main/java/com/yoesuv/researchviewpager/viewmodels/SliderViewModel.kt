package com.yoesuv.researchviewpager.viewmodels

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel

class SliderViewModel: ViewModel() {

    var fragmentNumber: ObservableField<String> = ObservableField()

    fun initData(number: String) {
        fragmentNumber.set(number)
    }

}