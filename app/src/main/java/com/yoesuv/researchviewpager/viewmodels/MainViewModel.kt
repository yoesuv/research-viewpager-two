package com.yoesuv.researchviewpager.viewmodels

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import com.yoesuv.researchviewpager.views.HorizontalActivity
import com.yoesuv.researchviewpager.views.VerticalActivity

class MainViewModel(application: Application) : AndroidViewModel(application) {

    fun clickHorizontal(view: View) {
        val ctx = view.context
        ctx.startActivity(HorizontalActivity.getInstance(ctx))
    }

    fun clickVertical(view: View) {
        val ctx = view.context
        ctx.startActivity(VerticalActivity.getInstance(ctx))
    }

}