package com.yoesuv.researchviewpager.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.material.tabs.TabLayoutMediator
import com.yoesuv.researchviewpager.R
import com.yoesuv.researchviewpager.adapters.ViewPagerAdapter
import com.yoesuv.researchviewpager.databinding.ActivityViewPagerHorizontalBinding

class HorizontalActivity: AppCompatActivity() {

    companion object {
        fun getInstance(context: Context): Intent {
            return Intent(context, HorizontalActivity::class.java)
        }
    }

    private lateinit var binding: ActivityViewPagerHorizontalBinding

    private lateinit var vPagerAdapter: ViewPagerAdapter
    private val tabData = listOf("Fragment 1", "Fragment 2", "Fragment 3", "Fragment 4")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_pager_horizontal)
        binding.lifecycleOwner = this

        setupViewPager()
    }

    private fun setupViewPager() {
        vPagerAdapter = ViewPagerAdapter(this, tabData)
        binding.vpHorizontal.adapter = vPagerAdapter
        TabLayoutMediator(binding.tlVpHorizontal, binding.vpHorizontal) { tab, position ->
            tab.text = tabData[position]
        }.attach()
    }

}