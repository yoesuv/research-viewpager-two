package com.yoesuv.researchviewpager.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.yoesuv.researchviewpager.databinding.FragmentSliderBinding
import com.yoesuv.researchviewpager.viewmodels.SliderViewModel

class FragmentSlider: Fragment() {

    companion object {
        private const val EXTRA_DATA_NUMBER = "extra_data_number"
        fun getInstance(number: String): Fragment {
            val bundle = Bundle()
            bundle.putString(EXTRA_DATA_NUMBER, number)
            val fragment = FragmentSlider().apply {
                arguments = bundle
            }
            return fragment
        }
    }

    private lateinit var binding: FragmentSliderBinding
    private lateinit var viewModel: SliderViewModel
    private var number: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.getString(EXTRA_DATA_NUMBER)?.let {
            number = it
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSliderBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        viewModel = ViewModelProvider(this).get(SliderViewModel::class.java)
        binding.slider = viewModel

        viewModel.initData(number)

        return binding.root
    }

}