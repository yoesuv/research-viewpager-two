package com.yoesuv.researchviewpager.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.yoesuv.researchviewpager.R
import com.yoesuv.researchviewpager.adapters.ViewPagerAdapter
import com.yoesuv.researchviewpager.databinding.ActivityViewPagerVerticalBinding

class VerticalActivity: AppCompatActivity() {

    companion object {
        fun getInstance(context: Context): Intent {
            return Intent(context, VerticalActivity::class.java)
        }
    }

    private lateinit var binding: ActivityViewPagerVerticalBinding

    private lateinit var vPagerAdapter: ViewPagerAdapter
    private val tabData = listOf("Fragment 1", "Fragment 2", "Fragment 3", "Fragment 4")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_pager_vertical)
        binding.lifecycleOwner = this

        setupViewPager()
    }

    private fun setupViewPager() {
        vPagerAdapter = ViewPagerAdapter(this, tabData)
        binding.vpVertical.adapter = vPagerAdapter
        binding.dotsVpVertical.setViewPager2(binding.vpVertical)
    }

}